with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "{{cookiecutter.project}}";

  buildInputs = [
    go_1_13.all
  ];

  shellHook = ''
  export GOPATH=/home/mukund/gocode/
  export PATH=$PATH:$GOPATH/bin
  '';
}
