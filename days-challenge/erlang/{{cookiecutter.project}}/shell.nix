with import <nixpkgs> {};
with pkgs.python37Packages;

stdenv.mkDerivation {
  name = "{{cookiecutter.project}}";

  buildInputs = [
    erlangR21
    elixir

    python37Packages.jupyter
  ];
}
