with import <nixpkgs> {};
with pkgs.python37Packages;


stdenv.mkDerivation {
  name = "{{cookiecutter.project}}";

  sec = null;
  SOURCE_DATE_EPOCH = 315532800;
  
  buildInputs = [
    python37Full
    python37Packages.virtualenvwrapper

    python37Packages.mypy
    python37Packages.jupyter
  ];

  shellHook = ''
  export WORKON_HOME=$HOME/.virtualenvs
  source `which virtualenvwrapper.sh`
  workon day_of_python
  '';
}
