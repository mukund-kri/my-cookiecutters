switch ReactDOM.querySelector("#root") {
| Some(root) => ReactDOM.render(<Greet name="John" />, root)
| None => ()
}