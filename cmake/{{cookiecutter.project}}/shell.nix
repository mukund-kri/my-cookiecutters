with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "{{ cookiecutter.project | toProjectName }}";
  src = ./.;
  
  buildInputs = [
    {% if cookiecutter.use_boost %} boost {% endif %}
  ];
  nativeBuildInputs = [ cmake ];

  shellHook = ''
  alias crun='cmake --build . && ./{{ cookiecutter.project | toProjectName }}'
  '';

}
