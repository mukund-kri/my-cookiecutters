import Dependencies._

ThisBuild / scalaVersion := "{{ cookiecutter.scala_version }}"
ThisBuild / organization := "{{ cookiecutter.organization }}"
ThisBuild / version := "{{ cookiecutter.version }}"

lazy val root = (project in file(".")).
  settings(
    name := "{{ cookiecutter.name }}",

    // TODO: Add your dependency here
    
    libraryDependencies ++= Seq(
      scalaTest % Test
      // TODO: Add your test dependencies here
    )
  )
