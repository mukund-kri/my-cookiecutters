// Custom CLI type for running examples
package in.net.mukund


import scala.util.CommandLineParser

enum Example:
    // TODO: List your examples here
    case example1, example2

// Implicit for converting Example to String
given CommandLineParser.FromString[Example] with
    def fromString(s: String): Example = Example.valueOf(s)

