with import <nixpkgs> {};

stdenv.mkDerivation {

  name = "{{cookiecutter.project}}";
  
  buildInputs = [
    m4
    ncurses

    fswatch
  ] ++ (
    with ocamlPackages; [
      ocaml
      
      ninja
      merlin
      opam
      utop
    ]
  );

  shellHook = ''
    eval $(opam env)  
  '';

}
