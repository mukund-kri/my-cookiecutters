with import <nixpkgs> {};


stdenv.mkDerivation {
  name = "resonml";
  buildInputs = [
    gcc
    glibc
    nodejs-10_x
  ] ++ (with ocamlPackages_4_02; [ ocaml ninja merlin ]);

  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"
    mkdir -p ./node_modules/.bin
    ln -s ~/node/resonml-bootstrap/node_modules/bs-platform/ ./node_modules/
    ln -s ~/node/resonml-bootstrap/node_modules/bs-platform/lib/bsb ./node_modules/.bin
    ln -s ~/node/resonml-bootstrap/node_modules/bs-platform/lib/bsc ./node_modules/.bin
    ln -s ~/node/resonml-bootstrap/node_modules/bs-platform/lib/bsrefmt ./node_modules/.bin
  '';
}
