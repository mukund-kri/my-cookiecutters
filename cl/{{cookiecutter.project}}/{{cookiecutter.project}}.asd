(defsystem "{{cookiecutter.project}}"
  :depends-on ()
  :components ((:module "src"
		:components
		((:file "main"))))
  :build-operation "asdf:program-op"
  :build-pathname "{{cookiecutter.project}}"
  :entry-point "{{cookiecutter.project}}::main")
